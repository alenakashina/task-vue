import Vue from 'vue'
import App from './App.vue'
import VueAxios from './plugins/axios'
//import Tree from './Tree'

Vue.use(VueAxios)
Vue.prototype.$axios = axios


new Vue({
  el: '#app',
  render: h => h(App)

})
